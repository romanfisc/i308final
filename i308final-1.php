<!doctype html>
<html>
<body>


<h2> I308 Final Project <h2>
<!--Semester Title "Spring 2016" does not have letter grades because this is the current semester and we want to show that our database can handle students who are in a semester and have not recieved grades yet.-->



<h3>1a</h3>
<b>Produce a roster for a *specified section* sorted by student’s last name, first name (5 points)</b><br>
<form action="1a.php" method="POST">
Select SectionID: <select name="SectID">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT sectionID from section;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($SectID);
	$SectID = $row['sectionID'];
	echo '<option value="'.$SectID.'">'.$SectID.'</option>';
}
?>
       </select>
<br>
<input type="submit" name="submit" value="Run Query 1a">
</form>



<form action="2b.php" method="post">
<h3>2b</h3>
<b>Produce a list of rooms that are equipped with *some feature* and are available at 6:30am on Monday. (10 points)</b><br>
Select Feature: <select name="Feature">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT distinct room_feature from features;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($Feature);
	$Feature = $row['room_feature'];
	echo '<option value="'.$Feature.'">'.$Feature.'</option>';
}
?>
		</select>
<br>
<input type="submit" value="Run Query 2b">
</form>



<form action="3b.php" method="post">
<h3>3b</h3>
<b>Produce a list of faculty who have never taught a *specified course*. (10 points)</b><br>
Select Course: <select name="Course">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT distinct course_title from course;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($Course);
	$Course = $row['course_title'];
	echo '<option value="'.$Course.'">'.$Course.'</option>';
}
?>
		</select>
<br>
<input type="submit" value ="Run Query 3b" ></br><br>
</form>



<form action="4c.php" method="post">
<h3>4c</h3>
<b>Produce a list of all students who took a course that had a prerequisite but the student had not taken the prerequisite. Include the semester, the course subject and number, and the grade the student received. (15 points)
<br><input type="submit" value ="Run Query 4c" ></br><br>
</form>



<form action="5a.php" method="post">
<h3>5a</h3>
<b>Produce a chronological list (transcript-like) of all courses taken by a *specified student*. Show grades earned. (5 points)</b><br>
Select a Student: <select name="StudentID">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT first_name, last_name, studentID from student;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($StudentID, $Student_first, $Student_last);
	$StudentID = $row['studentID'];
	$Student_first = $row['first_name'];
	$Student_last = $row['last_name'];

	echo '<option value="'.$StudentID.'">'.$Student_first.' '.$Student_last.'</option>';
}
?>
       </select>
<br>
<input type="submit" name="submit" value="Run Query 5a">
</form>



<form action="6c.php" method="post">
<h3>6c</h3>
<b>Produce a list of students and faculty who were in a *particular building* at a *particular time* on Thursday. Also include in the list faculty and advisors who have offices in that building. (15 points)</b><br>
Select a Building: <select name="Building">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT buildingID, building_name from building;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($Building, $Building_name);
	$Building = $row['buildingID'];
	$Building_name = $row['building_name'];

	echo '<option value="'.$Building.'">'.$Building_name.'</option>';
}
?>
       </select>
	   <br>
Select a Time: <select name="Time">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT distinct occupied_start_time from room order by occupied_start_time asc;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($Time);
	$Time = $row['occupied_start_time'];

	echo '<option value="'.$Time.'">'.$Time.'</option>';
}
?>
       </select>
	   <br>
<input type="submit" value ="Run Query 6c" ></br><br>
</form>



<form action="7a.php" method="POST">
<h3>7a</h3>
<b>Produce an alphabetical list of students with their majors who are advised by a *specified advisor* </b>
<br>
Select Advisor ID: <select name="AdvisorID">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT advisorID from advisors;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($adID);
	$adID = $row['advisorID'];
	echo '<option value="'.$adID.'">'.$adID.'</option>';
}
?>
       </select>
<br>
<input type="submit" name="submit" value="Run Query 7a">
</form>


<form action="8b.php" method="POST">
<h3>8b</h3>
<b>Produce an alphabetical list of students who have not attended during the two most recent semesters along with their parents’ phone number (10 points)</b>
<br>
<input type="submit" name="submit" value="Run Query 8b">
</form>



<form action="9b.php" method="POST">
<h3>9b</h3>
<b>Produce a list of students with hours earned who have met graduation requirements for a *specified major*.</b>
</br>
Select Major ID: <select name="majorID">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT majorID from majors;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($majID);
	$majID = $row['majorID'];
	echo '<option value="'.$majID.'">'.$majID.'</option>';
}
?>
       </select>
<br>
<input type="submit" name="submit" value="Run Query 9b">
</form>



<form action="additional1.php" method="POST">
<h3>Additional Query 1</h3>
<b>Produce a list of advisors with *specified expertise*.</b>
</br>
Select Expertise: <select name="Expertise">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT distinct expertise from expertise;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($Expertise);
	$Expertise = $row['expertise'];
	echo '<option value="'.$Expertise.'">'.$Expertise.'</option>';
}
?>
       </select>
<br>
<input type="submit" name="submit" value="Run Additional Query 1">
</form>
<br><br>



<form action="additional2.php" method="POST">
<h3>Additional Query 2</h3>
<b>Produce a list of students and their email address of *specified major*.</b>
</br>
Select Major: <select name="Major">
<?php
$conn = mysqli_connect("db.soic.indiana.edu","i308f16_team02","my+sql=i308f16_team02","i308f16_team02");
if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
 }
 $result = mysqli_query($conn,"SELECT distinct major_title from majors;");
 while ($row = mysqli_fetch_assoc($result)) {
	unset($Major);
	$Major = $row['major_title'];
	echo '<option value="'.$Major.'">'.$Major.'</option>';
}
?>
       </select>
<br>
<input type="submit" name="submit" value="Run Additional Query 2">
</form>
<br><br><br><br><br>



</body>
</html>
